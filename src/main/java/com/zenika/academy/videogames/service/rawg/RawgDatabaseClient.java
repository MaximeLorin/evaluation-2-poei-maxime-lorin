package com.zenika.academy.videogames.service.rawg;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Cette classe set à faire des requêtes HTTP vers l'api de rawg.io (https://rawg.io/apidocs et https://api.rawg.io/docs)
 */
@Component
public class RawgDatabaseClient {

    private final String apiKey; // TODO
    private final RestTemplate restTemplate;

    public RawgDatabaseClient(@Value("${video-games.apiKey}") String apiKey) {
        this.restTemplate = new RestTemplate();
        this.apiKey=apiKey;
    }

    public VideoGame getVideoGameFromName(String name)throws NullPointerException{
        RawgSearchResponse body = restTemplate
                .getForObject("https://api.rawg.io/api/games?key="+apiKey+"&search=" + name, RawgSearchResponse.class);
        if (body != null && body.results.size() > 0) {
            return body.results.get(0);
        }
        else {
           throw new NullPointerException();
        }
    }

    private static class RawgSearchResponse {
        private List<VideoGame> results;

        public List<VideoGame> getResults() {
            return results;
        }

        public void setResults(List<VideoGame> results) {
            this.results = results;
        }
    }
}
