package com.zenika.academy.videogames.service.rawg;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class NullPointerException extends Exception{
    @ExceptionHandler({ NullPointerException.class})
    public final ResponseEntity<ApiError> handleException(Exception e, WebRequest request){
        HttpHeaders headers=new HttpHeaders();

        if(e instanceof NullPointerException){
            HttpStatus status = HttpStatus.NOT_FOUND;
            NullPointerException nullPE = (NullPointerException) e;

            return wordWrongLengthException(nullPE, headers, status, request);
        }else {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return handleExceptionInternal(e, null, headers, status, request);
        }
    }

    private ResponseEntity<ApiError> handleExceptionInternal(Exception e, ApiError o, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, e, WebRequest.SCOPE_REQUEST);
        }

        return new ResponseEntity<>(o, headers, status);
    }

    protected ResponseEntity<ApiError> wordWrongLengthException(NullPointerException nullPE, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = Collections.singletonList(nullPE.getMessage());
        return handleExceptionInternal(nullPE, new ApiError(status.value(),"Ce jeux n'existe pas"), headers, status, request);
    }
}
