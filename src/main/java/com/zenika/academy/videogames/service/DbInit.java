package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.service.rawg.NullPointerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DbInit {
    @Autowired
    private VideoGamesService videoGamesService;

    @PostConstruct
    private void postConstruct() throws NullPointerException {
        videoGamesService.addVideoGame("Mario");
        videoGamesService.addVideoGame("Zelda");
    }
}
