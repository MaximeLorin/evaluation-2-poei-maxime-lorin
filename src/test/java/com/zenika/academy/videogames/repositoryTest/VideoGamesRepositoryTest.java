package com.zenika.academy.videogames.repositoryTest;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class VideoGamesRepositoryTest {
    @Autowired
    VideoGamesRepository videoGamesRepository;

    @Test
    void addOneFilmToRepo(){
        List<VideoGame> listOfGames=new ArrayList<>();
        Genre genre=new Genre("Comedie");

        List<Genre> genreList=new ArrayList<>();
        genreList.add(genre);

        VideoGame videoGame=new VideoGame(1L,"Drôlerie",genreList);
        listOfGames.add(videoGame);
        videoGamesRepository.save(videoGame);

        int size=videoGamesRepository.getAll().size();

        assertEquals(1,size);
        assertEquals(videoGamesRepository.getAll(),listOfGames);
    }

    @Test
    void addTwoFilmToRepo(){
        List<VideoGame> listOfGames=new ArrayList<>();
        Genre genre=new Genre("Comedie");

        List<Genre> genreList=new ArrayList<>();
        genreList.add(genre);

        VideoGame videoGame=new VideoGame(1L,"Drôlerie",genreList);
        VideoGame videoGameTwo=new VideoGame(2L,"Peureux",genreList);

        listOfGames.add(videoGame);
        listOfGames.add(videoGameTwo);

        videoGamesRepository.save(videoGame);
        videoGamesRepository.save(videoGameTwo);

        int size=videoGamesRepository.getAll().size();

        assertEquals(2,size);
        assertEquals(videoGamesRepository.getAll(),listOfGames);
    }
}
